$(document).ready(function(){

    var $header = $("header");
    var $sticky = $header.before($header.clone().addClass("sticky"));

    $(window).on("scroll", function(){
        var scrollFromTop = $(window).scrollTop();
        $("body").toggleClass("scroll", (scrollFromTop<150)); //by doing this we can add any animation to this when we scroll
    });

    // ### SMOOTH SCROLL ###
    $('.menu li a[href^="#"]').on('click', function(e){
        e.preventDefault();// by suing this we ignoring the default animations

        var target = $(this.hash);//this.hash reads the href attribute of this, and gets the part of the URL beginning with #.

        if(target.length){
            $('html, body').stop().animate({
                scrollTop:target.offset().top-60
            }, 1000);
        }
    });


    // ### MASONRY ###
    
    $(".grid").masonry({
        //options
        itemSelector:".grid-item",
        columnWidth:120,
        fitWidth:true,
        gutter:0
    }); //masonry js web site give some options

    // ### SLICK SLIDER ###

    $(".slider").slick({
        autoplay:true,
        autoplaySpeed:1500,
        arrows:true,
        dots:false,/*by default this is false*/
        centerMode:true,
        slidesToShow:3,
        fade:false, /*this works only there is one image*/
        prevArrow:'<button type="button" class="slick-prev">Previous</button>',
        nextArrow:'<button type="button" class="slick-next">Previous</button>',
    
        responsive: [
            {
                breakpoint: 990,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    }); //slick slider web site give some options 


    //RESPONSIVE MENU START

    var body = $('body');
    var menuTrigger = $('.js-menu-trigger');
    var mainOverlay = $('.js-main-overlay');

    //when hamburger icon click must appear menu from side bar
    menuTrigger.on('click', function(){
      body.addClass('menu-is-active');
    });

    //when hamburger icon click must appear menu from side bar
    mainOverlay.on('click', function(){
      body.removeClass('menu-is-active');
    });

    $('.menu li a').on('click', function(){
        $('body').removeClass("menu-is-active");
    });
});